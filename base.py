import json
import os
import string
import sys
import datetime


from twisted.internet import defer
from twisted.logger import Logger


log = Logger()

common_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', "python-agents-base-repo")
sys.path.append(common_path)

from common import MQTTService


TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'


def get_topic_params(topic_template):
    return [p[1] for p in string.Formatter().parse(topic_template)]


def get_topic_by_template(topic_template):
    params = get_topic_params(topic_template)
    return topic_template.format(**dict(zip(params, ['+'] * len(params))))


def get_params_dict(topic_template, param_values):
    param_names = get_topic_params(topic_template)
    return dict(zip(param_names, param_values))


def get_timestamp():
    return datetime.datetime.now().strftime(TIMESTAMP_FORMAT)


def parse_topic_func_factory(topic_exp):
    split_topic_exp = topic_exp.split('/')
    topic_len = len(split_topic_exp)
    param_indexes = [i for i, x in enumerate(split_topic_exp) if x == '+']

    def key(t):
        return [
            x if i not in param_indexes else '+' for i, x in enumerate(t)
        ]

    original_topic_key = key(split_topic_exp)

    def parse(topic):
        split_topic = topic.split('/')
        if len(split_topic) == topic_len and original_topic_key == key(split_topic):
            return [split_topic[param_index] for param_index in param_indexes]
    return parse


class ParseError(Exception):
    """"""


class BaseMQTTService(MQTTService):
    CONTENT_TYPE_JSON, CONTENT_TYPE_RAW, CONTENT_TYPE_CAN_BE_NONE = 'json', 'raw', 'can_be_none'

    def __init__(self, *args, **kwargs):
        self._subscription = {}
        self.topics = kwargs.pop('topics')
        self.parsers = {
            self.CONTENT_TYPE_JSON: self._parse_json,
            self.CONTENT_TYPE_RAW: lambda payload: payload,
            self.CONTENT_TYPE_CAN_BE_NONE: lambda payload: payload and self._parse_json(payload) or None
        }
        self.window_size = 2
        super(BaseMQTTService, self).__init__(*args, **kwargs)

    def add_window_size(self, delta):
        self.window_size += delta
        self.protocol.MAX_WINDOW = self.window_size
        self.protocol.setWindowSize(self.protocol.MAX_WINDOW)

    def subscribe(self, topic_template, handler):
        topic = get_topic_by_template(topic_template)

        self._subscription[topic_template] = (parse_topic_func_factory(topic), handler)

        def _logGrantedQoS(value):
            log.debug("response {value!r}", value=value)
            return True

        def _logAll(*args):
            log.debug("subscription to {topic} complete args={args!r}", topic=topic, args=args)

        self.add_window_size(1)

        d = self.protocol.subscribe(topic)
        d.addCallbacks(_logGrantedQoS, self.logFailure)
        d.addCallback(_logAll)
        return d

    def unsubscribe(self, topic_template):
        topic = get_topic_by_template(topic_template)
        self._subscription.pop(topic_template, None)
        d = self.protocol.unsubscribe(topic)
        d.addCallback(lambda *_: self.add_window_size(-1))
        return d

    def general_subscribe(self):
        return defer.DeferredList(map(lambda args: self.subscribe(*args), self.topics.items()))

    def _parse_json(self, payload):
        try:
            return json.loads(str(payload))
        except ValueError:
            log.debug('Invalid json format in payload {payload!r}', payload=payload)
            raise ParseError

    def onPublish(self, topic, payload, qos, dup, retain, msgId):
        for topic_template, (parser, handler) in self._subscription.items():
            content_type = self.CONTENT_TYPE_JSON
            if isinstance(handler, tuple):
                handler, content_type = handler
            param_values = parser(topic)
            if param_values is not None:
                try:
                    data = self.parsers[content_type](str(payload))
                except ParseError:
                    pass
                else:
                    handler(topic, get_params_dict(topic_template, param_values), data=data)
                break

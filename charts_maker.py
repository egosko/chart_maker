from twisted.internet import reactor

from service import ChartsMaker


from common import start_mqtt_base

from publisher import MQTTPublisherPool


def serv_creator(endpoint, factory, config):
    mqtt_pool_size = config["mqtt"].get("pool_size", 20)
    if mqtt_pool_size:
        publisher = MQTTPublisherPool(
            config["mqtt"]["host"], config["mqtt"]["port"],
            config["mqtt"]["user"], config["mqtt"]["pass"],
            config["mqtt"]["client_id"],
            mqtt_pool_size
        )
    else:
        publisher = None
    service = ChartsMaker(
        endpoint,
        factory,
        config,
        publisher=publisher
    )
    return service

if __name__ == '__main__':
    start_mqtt_base(serv_creator)
    reactor.suggestThreadPoolSize(20)
    reactor.run()

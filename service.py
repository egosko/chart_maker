import datetime
from collections import defaultdict
from functools import partial
import json
from multiprocessing import Pool

from twisted.internet import threads, defer, reactor
from twisted.logger import Logger

from base import BaseMQTTService, TIMESTAMP_FORMAT
from graph import get_combined_chart
import signal


def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)


log = Logger()


class MqttDataGetter(object):

    def __init__(self, mqtt):
        self.deferreds = defaultdict(list)
        self.mqtt = mqtt

    def get(self, topic):
        d_list = self.deferreds[topic]
        d = defer.Deferred()
        d_list.append(d)
        if len(d_list) == 1:
            self.mqtt.subscribe(topic, self._on_data)
        return d

    def _on_data(self, topic, _, data):
        self.mqtt.unsubscribe(str(topic))
        d_list = self.deferreds.pop(topic, None)
        if d_list:
            for d in d_list:
                try:
                    d.callback(data)
                except Exception:
                    log.failure('Error in mqtt getter callbacks')


class ChartsMaker(BaseMQTTService):

    def __init__(self, endpoint, factory, config, publisher=None):
        self.config = config
        self._publisher = publisher or self
        self._reset_state()
        self.pool = Pool(initializer=init_worker)
        signal.signal(signal.SIGINT, self.sigInt)

        topics = {
            str(config['mqtt']['channels']['charts']): self.on_chart
        }

        super(ChartsMaker, self).__init__(
            endpoint, factory,
            config["mqtt"]["user"], config["mqtt"]["pass"], config["mqtt"]["client_id"],
            config["mqtt"]['channels']['admin'], config.get("app_version"),
            topics=topics
        )

    def apply_async(self, f, *args):
        return self.pool.apply_async(f, args).get(9999999)

    def sigInt(self, *_):
        reactor.callFromThread(reactor.stop)
        reactor.callFromThread(self.pool.close)

    def _reset_state(self):
        self.history = MqttDataGetter(self)
        
    def onDisconnection(self, reason):
        self._reset_state()
        super(ChartsMaker, self).onDisconnection(reason)

    def _get_history(self, topic, period):
        days = {'last_hour': 1, 'last_day': 1, 'last_week': 7, 'last_month': 30}[period]
        if days == 1:
            return self.history.get(topic)
        else:
            return defer.gatherResults(
                map(self.history.get, ['/'.join([topic, str(i)]) for i in xrange(days)])
            )

    def get_dht_lumens_graph(self, data, title):
        sensors = 'dht_22_temperature', 'dht_22_humidity', 'tsl2561_lumens'
        names = ['Temperature', 'Humidity', 'Lumens']
        sensors_to_names = dict(zip(sensors, names))
        result = {
            'sensors': defaultdict(dict, {data['from']: {s: None for s in names}}),
            'from': datetime.datetime.strptime(data['from'], TIMESTAMP_FORMAT),
            'till': datetime.datetime.strptime(data['till'], TIMESTAMP_FORMAT),
            'resolution': data['resolution']
        }

        for sensor in sensors:
            for v in data['sensors'].get(sensor, []):
                result['sensors'][v['_id']][sensors_to_names[sensor]] = v['avg']

        def _log_error(failure, d=data):
            with open('errors.log', 'a+') as f:
                f.write(json.dumps(d) + '\n')
            failure.printTraceback()

        return threads.deferToThread(
            self.apply_async, get_combined_chart, result, *(names + [title])
        ).addErrback(_log_error)

    def on_chart(self, _, params, data):
        send_graph = partial(
            self._publisher.do_publish,
            str(data['dst_topic'].format(**params))
        )
        d = self.history.get(str(data['src_topic'].format(**params)))
        d.addCallback(partial(self.get_dht_lumens_graph, title=data['chart_title']))
        d.addCallback(send_graph)

        return d


from twisted.application.internet import ClientService, backoffPolicy
from twisted.internet import defer, endpoints, reactor

from twisted.logger import Logger


from mqtt.client.factory import MQTTFactory

log = Logger()


class MQTTPublisherService(ClientService):
    def __init__(self, endpoint, factory, user, passw, client_id):
        ClientService.__init__(self, endpoint, factory, retryPolicy=backoffPolicy())
        self.user = user
        self.passw = passw
        self.client_id = client_id

    def startService(self):
        log.info("starting publisher service ")
        self.whenConnected().addCallback(self.connectToBroker)
        ClientService.startService(self)

    @defer.inlineCallbacks
    def connectToBroker(self, protocol):
        self.protocol = protocol
        self.protocol.onDisconnection = self.onDisconnection
        self.protocol.setWindowSize(10)

        try:
            yield self.protocol.connect(
                self.client_id,
                keepalive=60,
                username=self.user,
                password=self.passw)
        except Exception as e:
            log.error("Connecting to broker raised {excp}",
                      excp=str(e))
        else:
            log.info("Connected and subscribed to broker")

    def onDisconnection(self, reason):
        log.error("Connection was lost, reason={r}", r=str(reason))
        self.whenConnected().addCallback(self.connectToBroker)

    def do_publish(self, topic, msg, qos=0, retain=False):
        return self.protocol.publish(topic, msg, qos, retain)

    @property
    def is_busy(self):
        return self.protocol.transport._isSendBufferFull()


def get_publisher(host, port, user, password, client_id):
    factory = MQTTFactory(MQTTFactory.PUBLISHER)
    broker = "tcp:%s:%s" % (host, str(port))
    endpoint = endpoints.clientFromString(reactor, broker)
    service = MQTTPublisherService(endpoint, factory,  user, password, client_id)
    service.startService()
    return service


class MQTTPublisherPool(object):

    def __init__(self, host, port, user, password, client_id, pool_size=30):
        self._pool = defer.DeferredQueue(pool_size)
        self._busy = []
        for i in range(pool_size):
            self._pool.put(
                get_publisher(host, port, user, password, '{}publihser{}'.format(client_id, i))
            )

    def release(self, publisher):
        if not publisher.is_busy:
            self._pool.put(publisher)
        else:
            reactor.callLater(0, self.release, publisher)

    def do_publish(self, *args, **kwargs):
        def _publish(p):
            def on_end(*_):
                self.release(p)
                return _

            d = p.do_publish(*args, **kwargs)
            d.addCallback(on_end)
            d.addErrback(on_end)

            return d

        d = self._pool.get()
        d.addCallback(_publish)
        return d

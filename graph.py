import io

import matplotlib
matplotlib.use('AGG')

from matplotlib import pyplot, gridspec, patches
from matplotlib.figure import Figure
from matplotlib.backends import backend_agg
import pandas


pyplot.style.use('fivethirtyeight')


def get_graph(data):

    df = (
        pandas.DataFrame(data, columns=['timestamp', 'max', 'min', 'avg'])
            .set_index('timestamp')
    )

    print df
    print df.dtypes

    plot = df.plot(color=['r', 'g', 'b'])
    figure = plot.get_figure()

    with io.BytesIO() as image_file:
        figure.savefig(image_file)
        graph = bytearray(image_file.getvalue())

    pyplot.close(figure)

    return graph


def style_chart(ax, data, x):
    ax.spines["top"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.tick_params(
        axis='both',
        which='both',
        left='off',
        right='off',
        bottom='off',
        length=0,
        labelsize=14
    )
    ylimit = data.min() - 5, data.max() + 5
    xlimit = x.min(), x.max()
    ax.set(
        adjustable='box-forced',
        ylim=ylimit,
        xlim=xlimit
    )


def get_combined_chart(data, key1, key2, key3, title):
    intevals = pandas.date_range(
        data['from'],
        data['till'],
        freq='{}min'.format(data['resolution'])
    )
    data_frame = pandas.DataFrame.from_dict(data['sensors'], orient='index')
    data_frame.index = pandas.to_datetime(data_frame.index)
    data_frame = data_frame.reindex(intevals, fill_value=None)
    figure = Figure(figsize=(14, 6))
    _ = backend_agg.FigureCanvas(figure)

    gs = gridspec.GridSpec(2, 1, height_ratios=[5, 1])

    ax = figure.add_subplot(gs[0])

    ax.set_title(title)
    ax.set_ylabel(key1, color='#FC4e2F')

    key1_data = getattr(data_frame, key1)
    key2_data = getattr(data_frame, key2)
    key3_data = getattr(data_frame, key3)
    style_chart(ax, key1_data, data_frame.index)
    ax2 = ax.twinx()
    ax2.set_ylabel(key2, color='#008ed3')
    ax2.grid(which='both', axis='y', color='red', linewidth=0, alpha=0.5)
    style_chart(ax2, key2_data, data_frame.index)

    key1_data.plot(color='#FC4e2F', ax=ax)
    key2_data.plot(color='#008ed3', ax=ax2)

    ax.grid(which='minor', color='grey', linestyle='-', alpha=0.5)
    ax.grid(which='major', color='grey', linestyle='-', alpha=0.5)
    ax.get_xticklines()[0].set_linewidth(0.1)

    ax = figure.add_subplot(gs[1])

    style_chart(ax, key3_data, data_frame.index)
    ax.spines["bottom"].set_visible(True)
    ax.spines["bottom"].set_color('black')
    ax.spines["bottom"].set_linewidth(1)

    key3_data.plot(color='#e4ad35', ax=ax)

    ax.grid(which='minor', color='grey', linestyle='-', alpha=0.5)

    figure.subplots_adjust(hspace=10)
    figure.tight_layout()
    ax.set_title(key3+':')

    logo = pyplot.imread('./logo.png')
    figure.figimage(logo, 500, 120, zorder=1, alpha=0.1)

    with io.BytesIO() as image_file:
        figure.savefig(image_file)
        graph = bytearray(image_file.getvalue())

    pyplot.close(figure)

    return graph
#
# import datetime
# import pymongo
# client = pymongo.MongoClient('localhost', 27017)
#
# db = client.mqtt_logger
#
#
# def get_data(device_id, sensor_name, start_datetime, end_datetime, resolution):
#     pipeline = [
#         {'$match': {
#             'sensor_type': sensor_name,
#             'device_id': device_id,
#             'timestamp': {
#                 '$gte': start_datetime,
#                 '$lt': end_datetime
#             }
#
#         }},
#         {'$group': {
#             '_id': {'$subtract': [
#                 '$timestamp',
#                 {'$mod': [
#                     {'$subtract': ['$timestamp', datetime.datetime(1970, 01, 01)]},
#                     60 * resolution * 1000
#                 ]}
#             ]},
#             'avg': {'$avg': "$sensor_value"},
#             'max': {'$max': "$sensor_value"},
#             'min': {'$min': "$sensor_value"}
#         }
#         }
#     ]
#     return db.sensors.aggregate(pipeline)
#
# data = list(get_data('SeeedWioA333333', 'dht_11_humidity',
#                datetime.datetime(2017, 8,5),
#                datetime.datetime(2017, 8,6), 5))
#
# import time
# t = time.time()
# chart = get_graph(data, datetime.datetime(2017, 8, 5), datetime.datetime(2017, 8, 6), 5)
# print time.time() - t
# with open('1.png', 'wb') as out:
#     out.write(chart.read())
